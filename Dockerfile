FROM node:alpine

RUN npm install -g serve

WORKDIR /app

COPY ./build ./

CMD serve -p $PORT -s ./
