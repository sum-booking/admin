const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

module.exports = function override(config, env) {
  config = injectBabelPlugin(['import', { libraryName: 'antd', style: true }], config);  // change importing css to less
  config = rewireLess.withLoaderOptions({
    modifyVars: { 
      "@primary-color": "#0A3F00",
      "@layout-header-background": "#031b00",
      "@font-size-base": "16px",
   },
  })(config, env);
  return config;
};
