import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage'
import Raven from "raven-js";
import createRavenMiddleware from "raven-for-redux";

import StartupActions from "./StartupRedux";

import reducers from './';
import sagas from '../Sagas';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['profile']
}

Raven.config(process.env.REACT_APP_SENTY_DSN).install();

const persistedReducer = persistReducer(persistConfig, reducers)

const sagaMiddleware = createSagaMiddleware({
  onError: (err) => {    
    Raven.captureException(err);
    if (process.env.NODE_ENV === "development") {
      console.error(err);
    }
  }
});
const ravenMiddleware = createRavenMiddleware(Raven);
const enhancers = [applyMiddleware(sagaMiddleware, ravenMiddleware)];

const store = createStore(persistedReducer, compose(...enhancers))
const persistor = persistStore(store, null, () => {
  StartupActions.startup();
});

sagaMiddleware.run(sagas);

export default () => {
  return { store, persistor }
}
