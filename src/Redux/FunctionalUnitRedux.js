import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  addFunctionalUnitsRequest: ['values'],
  addFunctionalUnitsFailed: ['values'],
  addFunctionalUnitsResponse: ['values'],
  getFunctionalUnitListRequest: null,
  getFunctionalUnitListResponse: ['functionalUnits'],
  getFunctionalUnitListFailed: ['message']
});

export const FunctionalUnitTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  adding: [],
  addFailedMessages: [],
  savedFunctionalUnits: [],
  functionalUnits: [],
  isFetchingFunctionalUnits: false,
  failedListMesage: null,
});

const addRequest = (state, { values }) => Immutable.merge(state, { adding: values.keys, savedFunctionalUnits: [], addFailedMessages: [] });
const addFailed = (state, { values }) => Immutable.merge(state, { addFailedMessages: values, adding: [] });
const addResponse = (state, { values }) => Immutable.merge(state, { savedFunctionalUnits: values, adding: [] });
const getListRequest = (state) => Immutable.merge(state, { isFetchingFunctionalUnits: true, failedListMesage: null });
const getListResponse = (state, { functionalUnits }) => Immutable.merge(state, { functionalUnits, isFetchingFunctionalUnits: false, failedListMesage: null });

const getListFailed = (state, { message }) => Immutable.merge(state, { failedListMesage: message, isFetchingFunctionalUnits: false });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_FUNCTIONAL_UNITS_REQUEST]: addRequest,
  [Types.ADD_FUNCTIONAL_UNITS_FAILED]: addFailed,
  [Types.ADD_FUNCTIONAL_UNITS_RESPONSE]: addResponse,
  [Types.GET_FUNCTIONAL_UNIT_LIST_REQUEST]: getListRequest,
  [Types.GET_FUNCTIONAL_UNIT_LIST_RESPONSE]: getListResponse,
  [Types.GET_FUNCTIONAL_UNIT_LIST_FAILED]: getListFailed,
});
