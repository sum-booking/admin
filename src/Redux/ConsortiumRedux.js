import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  addConsortiumRequest: ['code', 'name'],
  addConsortiumFailed: ['message'],
  addConsortiumResponse: null,
  getConsortiumListRequest: null,
  getConsortiumListResponse: ['consortiums'],
  getConsortiumListFailed: ['message'],
  setSelectedConsortiumCode: ['code']
})

export const ConsortiumTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  isAdding: false,
  addFailedMessage: null,
  consortiums: [],
  isFetchingConsortiums: false,
  failedListMesage: null,
  selectedConsortium: null,
});

const addRequest = (state) => Immutable.merge(state, { isAdding: true, addFailedMessage: null });
const addFailed = (state, { message }) => Immutable.merge(state, { addFailedMessage: message, isAdding: false });
const addResponse = (state) => Immutable.merge(state, { addFailedMessage: null, isAdding: false });
const getListRequest = (state) => Immutable.merge(state, { isFetchingConsortiums: true, failedListMesage: null });
const getListResponse = (state, { consortiums }) => {
  let selectedConsortium = state.selectedConsortium;
  if (state.consortiums.length === 0 && consortiums.length > 0) {
    selectedConsortium = consortiums[0];
  }
  return Immutable.merge(state, { consortiums, isFetchingConsortiums: false, failedListMesage: null, selectedConsortium })
}

const getListFailed = (state, { message }) => Immutable.merge(state, { failedListMesage: message, isFetchingConsortiums: false });
const setSelectedConsortium = (state, { code }) => {
  const consortium = state.consortiums.find(item => item.code === code);
  return Immutable.merge(state, { selectedConsortium: consortium });
}

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_CONSORTIUM_REQUEST]: addRequest,
  [Types.ADD_CONSORTIUM_FAILED]: addFailed,
  [Types.ADD_CONSORTIUM_RESPONSE]: addResponse,
  [Types.GET_CONSORTIUM_LIST_REQUEST]: getListRequest,
  [Types.GET_CONSORTIUM_LIST_RESPONSE]: getListResponse,
  [Types.GET_CONSORTIUM_LIST_FAILED]: getListFailed,
  [Types.SET_SELECTED_CONSORTIUM_CODE]: setSelectedConsortium
});
