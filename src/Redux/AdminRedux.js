import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  addAdminRequest: ['firstName', 'lastName', 'email', 'phoneNumber', 'consortiumCodes'],
  addAdminFailed: ['message'],
  addAdminResponse: null,
  getAdminListRequest: null,
  getAdminListResponse: ['admins'],
  getAdminListFailed: ['message']
})

export const AdminTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  isAdding: false,
  addFailedMessage: null,
  admins: [],
  isFetchingAdmins: false,
  failedListMesage: null,
});

const addRequest = (state) => Immutable.merge(state, { isAdding: true, addFailedMessage: null });
const addFailed = (state, { message }) => Immutable.merge(state, { addFailedMessage: message, isAdding: false });
const addResponse = (state) => Immutable.merge(state, { addFailedMessage: null, isAdding: false });
const getListRequest = (state) => Immutable.merge(state, { isFetchingAdmins: true, failedListMesage: null });
const getListResponse = (state, { admins }) => Immutable.merge(state, { admins, isFetchingAdmins: false, failedListMesage: null });
const getListFailed = (state, { message }) => Immutable.merge(state, { failedListMesage: message, isFetchingAdmins: false });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_ADMIN_REQUEST]: addRequest,
  [Types.ADD_ADMIN_FAILED]: addFailed,
  [Types.ADD_ADMIN_RESPONSE]: addResponse,
  [Types.GET_ADMIN_LIST_REQUEST]: getListRequest,
  [Types.GET_ADMIN_LIST_RESPONSE]: getListResponse,
  [Types.GET_ADMIN_LIST_FAILED]: getListFailed,
});
