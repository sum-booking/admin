import { combineReducers } from 'redux';

export default combineReducers({
    profile: require('./ProfileRedux').reducer,
    consortium: require('./ConsortiumRedux').reducer,
    admin: require('./AdminRedux').reducer,
    functionalUnit: require('./FunctionalUnitRedux').reducer,
    user: require('./UserRedux').reducer,
    facility: require('./FacilityRedux').reducer,
    booking: require('./BookingRedux').reducer,
});
