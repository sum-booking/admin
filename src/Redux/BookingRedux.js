import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  getBookingListRequest: ['from', 'to'],
  getBookingListResponse: ['bookings'],
  getBookingListFailed: ['message']
});

export const BookingTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  bookings: [],
  isFetchingBookings: false,
  failedListMesage: null,
});

const getListRequest = (state) => Immutable.merge(state, { isFetchingBookings: true, failedListMesage: null });
const getListResponse = (state, { bookings }) => Immutable.merge(state, { bookings, isFetchingBookings: false, failedListMesage: null });
const getListFailed = (state, { message }) => Immutable.merge(state, { failedListMesage: message, isFetchingBookings: false });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_BOOKING_LIST_REQUEST]: getListRequest,
  [Types.GET_BOOKING_LIST_RESPONSE]: getListResponse,
  [Types.GET_BOOKING_LIST_FAILED]: getListFailed,
});
