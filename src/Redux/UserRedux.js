import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  addUserRequest: ['firstName', 'lastName', 'email', 'phoneNumber', 'functionalUnitCode', 'sendWelcomingMail'],
  addUserFailed: ['message'],
  addUserResponse: null,
  getUserListRequest: null,
  getUserListResponse: ['users'],
  getUserListFailed: ['message'],
  resendUserAuthLinkRequest: ['functionalUnitCode', 'userId'],
  resendUserAuthLinkResponse: null,
  resendUserAuthLinkFailed: ['message']
})

export const UserTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  isAdding: false,
  addFailedMessage: null,
  users: [],
  isFetchingUsers: false,
  failedListMesage: null,
  isResendingAuthLink: false,
  authLinkFailedMessage: null,
});

const addRequest = (state) => Immutable.merge(state, { isAdding: true, addFailedMessage: null });
const addFailed = (state, { message }) => Immutable.merge(state, { addFailedMessage: message, isAdding: false });
const addResponse = (state) => Immutable.merge(state, { addFailedMessage: null, isAdding: false });
const getListRequest = (state) => Immutable.merge(state, { isFetchingUsers: true, failedListMesage: null });
const getListResponse = (state, { users }) => Immutable.merge(state, { users, isFetchingUsers: false, failedListMesage: null });
const getListFailed = (state, { message }) => Immutable.merge(state, { failedListMesage: message, isFetchingUsers: false });
const resendAuthLinkRequest = (state) => Immutable.merge(state, { isResendingAuthLink: true, authLinkFailedMessage: null });
const resendAuthLinkResponse = (state) => Immutable.merge(state, { isResendingAuthLink: false, authLinkFailedMessage: null });
const resendAuthLinkError = (state, { message }) => Immutable.merge(state, { isResendingAuthLink: false, authLinkFailedMessage: message });



export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_USER_REQUEST]: addRequest,
  [Types.ADD_USER_FAILED]: addFailed,
  [Types.ADD_USER_RESPONSE]: addResponse,
  [Types.GET_USER_LIST_REQUEST]: getListRequest,
  [Types.GET_USER_LIST_RESPONSE]: getListResponse,
  [Types.GET_USER_LIST_FAILED]: getListFailed,
  [Types.RESEND_USER_AUTH_LINK_REQUEST]: resendAuthLinkRequest,
  [Types.RESEND_USER_AUTH_LINK_RESPONSE]: resendAuthLinkResponse,
  [Types.RESEND_USER_AUTH_LINK_FAILED]: resendAuthLinkError
});
