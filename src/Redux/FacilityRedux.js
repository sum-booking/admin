import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'

const { Types, Creators } = createActions({
  addFacilityRequest: ['values'],
  addFacilityFailed: ['message'],
  addFacilityResponse: [],
  getFacilityListRequest: null,
  getFacilityListResponse: ['facilities'],
  getFacilityListFailed: ['message']
});

export const FacilityTypes = Types;
export default Creators;

const INITIAL_STATE = Immutable({
  isAdding: false,
  addFailedMessage: null,
  facilities: [],
  isFetchingFacilities: false,
  failedListMesage: null,
});

const addRequest = (state, { values }) => Immutable.merge(state, { isAdding: true, addFailedMessage: null });
const addFailed = (state, { message }) => Immutable.merge(state, { addFailedMessage: message, isAdding: false });
const addResponse = (state) => Immutable.merge(state, { addFailedMessage: null, isAdding: false });
const getListRequest = (state) => Immutable.merge(state, { isFetchingFacilities: true, failedListMesage: null });
const getListResponse = (state, { facilities }) => Immutable.merge(state, { facilities, isFetchingFacilities: false, failedListMesage: null });

const getListFailed = (state, { message }) => Immutable.merge(state, { failedListMesage: message, isFetchingFacilities: false });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ADD_FACILITY_REQUEST]: addRequest,
  [Types.ADD_FACILITY_FAILED]: addFailed,
  [Types.ADD_FACILITY_RESPONSE]: addResponse,
  [Types.GET_FACILITY_LIST_REQUEST]: getListRequest,
  [Types.GET_FACILITY_LIST_RESPONSE]: getListResponse,
  [Types.GET_FACILITY_LIST_FAILED]: getListFailed,
});
