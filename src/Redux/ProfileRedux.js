import { createActions, createReducer } from 'reduxsauce';
import Immutable from 'seamless-immutable'
import decode from 'jwt-decode';

const namespace = 'https://sumbooking.com/';

const { Types, Creators } = createActions({
  login: [
    'accessToken', 'idToken', 'expiresAt'
  ],
  logout: null,
  updateToken: [
    'accessToken', 'idToken', 'expiresAt'
  ],
  toggleCollapsedMenu: null,
  error: ['message'],
  setMenuState: ['collapsedMenu']
});

export const ProfileTypes = Types;
export default Creators;

export const INITIAL_STATE = Immutable({
  accessToken: null,
  idToken: null,
  expiresAt: null,
  pictureUrl: null,
  isRoot: false,
  consortiumCodes: [],
  collapsedMenu: false,
  email: null,
  name: null,
});

const login = (state, { accessToken, idToken, expiresAt }) => Immutable.merge(state, {
  accessToken,
  idToken,
  expiresAt,
  pictureUrl: decode(idToken)['picture'],
  isRoot: decode(idToken)[`${namespace}isRoot`] === "true",
  consortiumCodes: decode(idToken)[`${namespace}/consortiumCodes`],
  email: decode(idToken)['email'],
  name: decode(idToken)['name'],
});
const logout = () => INITIAL_STATE;
const toggleCollapsedMenu = (state) => Immutable.merge(state, {
  collapsedMenu: !state.collapsedMenu
});
const setMenuState = (state, { collapsedMenu }) => Immutable.merge(state, { collapsedMenu });

export const reducer = createReducer(INITIAL_STATE, {
  [Types.LOGIN]: login,
  [Types.LOGOUT]: logout,
  [Types.UPDATE_TOKEN]: login,
  [Types.TOGGLE_COLLAPSED_MENU]: toggleCollapsedMenu,
  [Types.SET_MENU_STATE]: setMenuState,
});

export const isAuthenticated = (state) => {
  return new Date().getTime() < state.profile.expiresAt;
}
