import React, {Component} from 'react';
import loading from './loading.svg';
import {Redirect} from "react-router-dom";
import {connect} from "react-redux";
import Raven from "raven-js";

import Auth from "../../Services/Auth";
import ProfileActions from "../../Redux/ProfileRedux";

class Callback extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: false
    }

    this.handleAuthentication();
  }

  handleAuthentication = () => {
    const state = sessionStorage.getItem("auth0_state");
    const nonce = sessionStorage.getItem("auth0_nonce");
    Auth.parseHash({
      state,
      nonce
    }, (err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
      } else if (err) {
        Raven.captureException(`Error handling authentication: ${JSON.stringify(err)}`);
        this
          .props
          .profileError(err);
      }
      sessionStorage.removeItem("auth0_state");
      sessionStorage.removeItem("auth0_nonce");
    });
  }

  setSession = (authResult) => {
    // Set the time that the access token will expire at
    let expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    this
      .props
      .login(authResult.accessToken, authResult.idToken, expiresAt);
    this.setState({redirect: true});
  }

  render() {
    const style = {
      position: 'absolute',
      display: 'flex',
      justifyContent: 'center',
      height: '100vh',
      width: '100vw',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'white'
    }
    return this.state.redirect
      ? <Redirect to="/"/>
      : <div style={style}>
        <img src={loading} alt="loading"/>
      </div>

  }
}

const mapDispatchToProps = (dispatch) => ({
  profileError: (err) => dispatch(ProfileActions.error(err)),
  login: (accessToken, idToken, expiresAt) => dispatch(ProfileActions.login(accessToken, idToken, expiresAt))
})

export default connect(null, mapDispatchToProps)(Callback);
