import React, {Component} from "react";
import Guid from "guid";

import Auth from "../../Services/Auth";

const randomString = (length) => {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

class Login extends Component {
  componentDidMount() {
    const state = Guid.create();
    const nonce = randomString(32);
    sessionStorage.setItem("auth0_state", state);
    sessionStorage.setItem("auth0_nonce", nonce);
    Auth.authorize({state, nonce});
  }
  render() {
    return <div/>
  }
}

export default Login;
