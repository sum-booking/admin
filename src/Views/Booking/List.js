import React, { Component } from "react";
import { Table, Card } from "antd";
import { connect } from "react-redux";
import moment from "moment";

import BookingActions from "../../Redux/BookingRedux";

import SearchForm from "./Components/SearchForm";

class BookingList extends Component {
  state = {
    from: null,
    to: null
  }

  columns = [
    {
      title: 'Fecha',
      dataIndex: 'bookDate',
      key: 'bookDate'
    }, {
      title: 'Amenitie',
      dataIndex: 'facilityName',
      key: 'facilityName'
    }, {
      title: 'Unidad',
      dataIndex: 'functionalUnitDesignation',
      key: 'functionalUnitDesignation'
    }, {
      title: 'Tipo de reserva', 
      render: (text, record) => {
        switch(record.strategy) {
          case "MidDayAndNight":
            return record.isMidDay
              ? <span>Mediodia</span>
              : <span>Noche</span>
          case "NightOnly":
            return <span>Noche</span>;
          default:
            throw new Error("Reservation type not implemented for booking list");
        }
      }
    }
  ]

  componentDidMount() {
    if (this.props.selectedConsortium !== null) {
      this.props.getBookings(this.state.from, this.state.to);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.selectedConsortium !== prevProps.selectedConsortium) {
      this.props.getBookings(this.state.from, this.state.to);
    }
  }  

  render() {
    const data = this.props.bookings.map((item, key) => {
      const tmp = item.asMutable();
      tmp.key = key;
      tmp.createdOn = moment(tmp.createdOn).format('MMMM Do YYYY, HH:mm');
      return tmp;
    })
    return (
      <Card>
      <SearchForm search={this.props.getBookings} period={(from, to) => this.setState({from, to})} />
        <Table dataSource={data} columns={this.columns} loading={this.props.fetching} size="small" scroll={{ x: 700 }} />
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  bookings: state.booking.bookings,
  fetching: state.booking.isFetchingBookings,
  selectedConsortium: state.consortium.selectedConsortium,
});

const mapDispatchToProps = dispatch => ({
  getBookings: (from, to) => dispatch(BookingActions.getBookingListRequest(from, to)),
})

export default connect(mapStateToProps, mapDispatchToProps)(BookingList);
