import React, { Component } from "react";
import { Form, Row, Col, DatePicker } from "antd";
import moment from 'moment';

class SearchForm extends Component {
  onPeriodChange = (values) => {
    const from = values[0].format();
    const to = values[1].format();
    this.props.period(from, to);
    this.props.search(from, to);
  }

  componentDidMount() {
    const today = new moment();
    const startOfMonth = new moment().startOf('month');
    this.props.form.setFieldsValue({
      period: [startOfMonth, today]
    });
    const from = startOfMonth.format();
    const to = today.format();
    this.props.period(from, to);
    this.props.search(from, to);
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
    };
    return(
      <Form
        className="ant-advanced-search-form"
      >
      <Row gutter={24}>
      <Col span={24} >
          <Form.Item label="Período:" {...formItemLayout}>
            {getFieldDecorator("period")(
              <DatePicker.RangePicker onChange={this.onPeriodChange} allowClear={false} />
            )}
          </Form.Item>
        </Col>
        </Row>
      </Form>
    )
  }
}

export default Form.create()(SearchForm);
