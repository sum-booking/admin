import React, { Component } from "react";
import { Form, Input, Button, Card, Alert } from 'antd';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import ConsortiumActions from "../../Redux/ConsortiumRedux";

const FormItem = Form.Item;

class ConsortiumForm extends Component {
  state = {
    conflict: false,
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this
      .props
      .form
      .validateFields((err, values) => {
        if (!err) {
          this.props.save(values.code, values.name);
        }
      });
  }
  componentDidUpdate(prevProps) {
    if (prevProps.isAdding && !this.props.isAdding && !this.props.message) {
      this.props.history.replace("/consortium");
    }
    if (this.props.message !== prevProps.message && this.props.message === 409) {
      this.setState({ conflict: true });
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { isAdding } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 6}
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 10}
      }
    };
    const submitButtonLayout = {
      wrapperCol: {
        xs: {span: 6, offset: 8},
        sm: {span: 10, offset: 6}
      }
    };
    return (
      <Card title={<span className="text-bold">Agregar consorcio</span>} type="inner">
        {this.state.conflict &&
          <Alert message="El código ya existe para otro consorcio" type="error" showIcon />
        }
        <Form onSubmit={this.handleSubmit} style={{ marginTop: 20 }}>
          <FormItem label="Código:" {...formItemLayout}>
            {getFieldDecorator('code', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un código'
                }
              ]
            })(<Input placeholder="Ingrese un código" disabled={isAdding} maxLength={8} autoFocus />)}
          </FormItem>
          <FormItem label="Nombre:" {...formItemLayout}>
            {getFieldDecorator('name', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un nombre'
                }
              ]
            })(<Input placeholder="Ingrese un nombre" maxLength={500} disabled={isAdding} />)}
          </FormItem>
          <FormItem {...submitButtonLayout}>
            <Button type="primary" htmlType="submit" disabled={isAdding}>Guardar</Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  message: state.consortium.addFailedMessage,
  isAdding: state.consortium.isAdding
});

const maptDispatchToProps = dispatch => ({
  save: (code, name) => dispatch(ConsortiumActions.addConsortiumRequest(code, name))
});

export default withRouter(connect(mapStateToProps, maptDispatchToProps)(Form.create()(ConsortiumForm)));
