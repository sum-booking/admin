import React, { Component } from "react";
import { Form, Input, Button, Card, Alert, Select } from 'antd';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import AdminActions from "../../Redux/AdminRedux";

const FormItem = Form.Item;

class AdminForm extends Component {
  state = {
    conflict: false,
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this
      .props
      .form
      .validateFields((err, values) => {
        if (!err) {
          this.props.save(values.firstName, values.lastName, values.email,
            values.phoneNumber, values.consortiumCodes);
        }
      });
  }
  componentDidUpdate(prevProps) {
    if (prevProps.isAdding && !this.props.isAdding && !this.props.message) {
      this.props.history.replace("/admin");
    }
    if (this.props.message !== prevProps.message && this.props.message === 409) {
      this.setState({ conflict: true });
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { isAdding } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 6}
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 10}
      }
    };
    const submitButtonLayout = {
      wrapperCol: {
        xs: {span: 6, offset: 8},
        sm: {span: 10, offset: 6}
      }
    };
    return (
      <Card title={<span className="text-bold">Agregar administrador</span>} type="inner">
        {this.state.conflict &&
          <Alert message="El correo electrónico ya existe para otro administrador" type="error" showIcon />
        }
        <Form onSubmit={this.handleSubmit} style={{ marginTop: 20 }}>
          <FormItem label="Nombre:" {...formItemLayout}>
            {getFieldDecorator('firstName', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un nombre'
                }
              ]
            })(<Input placeholder="Ingrese un nombre" maxLength={500} disabled={isAdding} autoFocus />)}
          </FormItem>
          <FormItem label="Apellido:" {...formItemLayout}>
            {getFieldDecorator('lastName', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un apellido'
                }
              ]
            })(<Input placeholder="Ingrese un apellido" maxLength={500} disabled={isAdding} />)}
          </FormItem>
          <FormItem label="Correo electrónico:" {...formItemLayout}>
            {getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un correo electrónico'
                }
              ]
            })(<Input placeholder="Ingrese un correo electrónico" disabled={isAdding} maxLength={500} type="email" />)}
          </FormItem>
          <FormItem label="Teléfono:" {...formItemLayout}>
            {getFieldDecorator('phoneNumber', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un teléfono'
                }
              ]
            })(<Input placeholder="Ingrese un teléfono" disabled={isAdding} maxLength={500} />)}
          </FormItem>
          <FormItem label="Consorcios a administrar" {...formItemLayout}>
            {getFieldDecorator('consortiumCodes', {
              rules: [
                {
                  required: true,
                  message: 'Debe seleccionar al menos un consorcio'
                }
              ]
            })(<Select
              mode="tags"
              style={{ width: '100%' }}
              placeholder="Elegir consorcios"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
              {this.props.consortiums.map((item, key) => 
              <Select.Option key={key} disabled={item.administratorId !== null} value={item.code}>{item.name}</Select.Option>)}
            </Select>)}
          </FormItem>
          <FormItem {...submitButtonLayout}>
            <Button type="primary" htmlType="submit" disabled={isAdding}>Guardar</Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  consortiums: state.consortium.consortiums,
  message: state.admin.addFailedMessage,
  isAdding: state.admin.isAdding
});

const maptDispatchToProps = dispatch => ({
  save: (firstname, lastName, email, phoneNumber, consortiumCodes) =>
    dispatch(AdminActions.addAdminRequest(firstname, lastName, email, phoneNumber, consortiumCodes))
});

export default withRouter(connect(mapStateToProps, maptDispatchToProps)(Form.create()(AdminForm)));
