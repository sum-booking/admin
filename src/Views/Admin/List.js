import React, { Component } from "react";
import { Table, Card } from "antd";
import { connect } from "react-redux";
import moment from "moment";

const columns = [
  {
    title: 'Nombre',
    dataIndex: 'name',
    key: 'name',
    fixed: 'left'
  },
  {
    title: 'Correo',
    dataIndex: 'email',
    key: 'email'
  },
  {
    title: 'Teléfono',
    dataIndex: 'phoneNumber',
    key: 'phoneNumber'
  },
  {
    title: 'Consorcios',
    dataIndex: 'consortiums',
    key: 'consortiums',
    render: (text, record) => {
      return text.map((item, key) =>
        <span key={key}>{item.name}<br/></span>
      );
    }
  }
]

class AdminList extends Component {
  render() {
    const data = this.props.admins.map((item, key) => {
      const tmp = item.asMutable();
      tmp.key = key;
      tmp.createdOn = moment(tmp.createdOn).format('MMMM Do YYYY, HH:mm');
      tmp.name = `${tmp.firstName} ${tmp.lastName}`;
      return tmp;
    })
    return (
      <Card>
        <Table dataSource={data} columns={columns} size="small" scroll={{ x: 700 }} />
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  admins: state.admin.admins
});

export default connect(mapStateToProps)(AdminList);
