import React, { Component } from "react";
import { Table, Card } from "antd";
import { connect } from "react-redux";
import moment from "moment";

import FunctionalUnitActions from "../../Redux/FunctionalUnitRedux";

const columns = [
  {
    title: 'Código',
    dataIndex: 'code',
    key: 'code'
  }, {
    title: 'Designación',
    dataIndex: 'designation',
    key: 'designation'
  }
]

class FunctionalUnitList extends Component {
  componentDidMount() {
    if (this.props.selectedConsortium !== null) {
      this.props.getFunctionalUnits();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.selectedConsortium !== prevProps.selectedConsortium) {
      this.props.getFunctionalUnits();
    }
  }

  render() {
    const data = this.props.functionalUnits.map((item, key) => {
      const tmp = item.asMutable();
      tmp['key'] = key;
      tmp.createdOn = moment(tmp.createdOn).format('MMMM Do YYYY, HH:mm');
      return tmp;
    })
    return (
      <Card>
        <Table dataSource={data} columns={columns} loading={this.props.fetching} size="small" />
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  functionalUnits: state.functionalUnit.functionalUnits,
  selectedConsortium: state.consortium.selectedConsortium,
  fetching: state.functionalUnit.isFetchingFunctionalUnits,
});

const mapDispatchToProps = dispatch => ({
  getFunctionalUnits: () => dispatch(FunctionalUnitActions.getFunctionalUnitListRequest()),
})

export default connect(mapStateToProps, mapDispatchToProps)(FunctionalUnitList);
