import React, { Component } from "react";
import { Form, Input, Button, Card, Alert, Icon, Row, Col, Spin } from 'antd';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import FunctionalUnitActions from "../../Redux/FunctionalUnitRedux";

const FormItem = Form.Item;

let uuid = 0;

class FunctionalUnitForm extends Component {
  state = {
    alreadySaved: [],
  }
  remove = (k) => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    // We need at least one functional unit
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  }

  add = () => {
    const { form } = this.props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(uuid);
    uuid++;
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys,
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { saved, form } = this.props;
    const { alreadySaved } = this.state;
    const mergedSaved = alreadySaved.concat(saved);
    this.setState({ alreadySaved: mergedSaved });
    form.validateFields((err, values) => {
      if (!err) {
        const keys = values.keys.filter(x => mergedSaved.findIndex(s => s.key === x) === -1);
        this.props.save({ keys, codes: values.code, designations: values.designation });
      }
    });
  }

  componentDidMount() {
    this.add();
  }

  hasError = (key) => {
    return this.props.errors.findIndex(x => x.key === key) !== -1;
  }

  isSaved = (key) => {
    return this.props.saved.findIndex(x => x.key === key) !== -1 ||
      this.state.alreadySaved.findIndex(x => x.key === key) !== -1;
  }

  isAdding = (key) => {
    return this.props.adding.findIndex(x => x === key) !== -1
  }

  render() {
    const { getFieldDecorator, getFieldValue } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 6}
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 18}
      }
    };
    const submitButtonLayout = {
      wrapperCol: {
        span: 24,
        offset: 0
      }
    };
    const selectedConsortiumName = this.props.selectedConsortium ? this.props.selectedConsortium.name : null;
    getFieldDecorator('keys', { initialValue: [] });
    const keys = getFieldValue('keys');
    const formItems = keys.map((k, index) => {
      return <div key={k}>
        {this.hasError(k) &&
          <Alert message="El código ya existe para otra unidad funcional" type="error" showIcon />
        }
        <Row >
          <Col sm={8} xs={24} >
            <FormItem label="Código:" {...formItemLayout} key={k}>
              {getFieldDecorator(`code[${k}]`, {
                rules: [
                  {
                    required: true,
                    message: 'Debe ingresar un código'
                  }
                ]
              })(<Input placeholder="Ingrese un código" disabled={this.isAdding(k) || this.isSaved(k)} maxLength={8} autoFocus />)}
            </FormItem>
          </Col>
          <Col sm={14} xs={24}>
            <FormItem label="Designación:" {...formItemLayout}>
              {getFieldDecorator(`designation[${k}]`, {
                rules: [
                  {
                    required: true,
                    message: 'Debe ingresar una designación'
                  }
                ]
              })(<Input placeholder="Ingrese una designación" maxLength={500} disabled={this.isAdding(k) || this.isSaved(k)} />)}
            </FormItem>
          </Col>
          <Col sm={2} xs={24}  >
            {this.isAdding(k) && !this.isSaved(k) &&
              <Spin className="dynamic-spin fu-icon"  />
            }
            {this.isSaved(k) &&
              <Icon
                className="dynamic-add-ok fu-icon"
                type="check-circle"
              />
            }
            {this.hasError(k) &&
              <Icon
                className="dynamic-add-fail fu-icon"
                type="exclamation-circle"
              />
            }
            {keys.length > 1 && !this.isAdding(k) && !this.isSaved(k) && !this.hasError(k) ? (
              <Icon
                className="dynamic-delete-button fu-icon"
                type="minus-circle-o"
                disabled={keys.length === 1}
                onClick={() => this.remove(k)}
              />
            ) : null}
          </Col>
        </Row>
      </div>
    });
    return (
      <Card title={<span className="text-bold">Agregar unidad funcional</span>} type="inner">
        <span className="text-bold">Consorcio: {selectedConsortiumName}</span>
        <Form onSubmit={this.handleSubmit} style={{ marginTop: 20 }}>
          {formItems}
          <FormItem {...submitButtonLayout}>
            <Button type="dashed" onClick={this.add} style={{ width: '100%' }}>
              <Icon type="plus" /> Agregar unidad funcional
          </Button>
          </FormItem>
          <FormItem {...submitButtonLayout}>
            <Button type="primary" htmlType="submit">Guardar</Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  selectedConsortium: state.consortium.selectedConsortium,
  errors: state.functionalUnit.addFailedMessages,
  saved: state.functionalUnit.savedFunctionalUnits,
  adding: state.functionalUnit.adding
});

const maptDispatchToProps = dispatch => ({
  save: (values) => dispatch(FunctionalUnitActions.addFunctionalUnitsRequest(values))
});

export default withRouter(connect(mapStateToProps, maptDispatchToProps)(Form.create()(FunctionalUnitForm)));
