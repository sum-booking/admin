import React, { Component } from "react";
import { Table, Card, Tooltip, Icon } from "antd";
import { connect } from "react-redux";
import moment from "moment";

import { limitStrategies, scheduleStrategies, cancellationPolicies, anticipationPolicies } from "../../Services/Strategies";
import FacilityActions from "../../Redux/FacilityRedux";

const columns = [
  {
    title: 'Nombre',
    dataIndex: 'name',
    key: 'name',
    fixed: 'left'
  }, {
    title: 'Disponibilidad',
    dataIndex: 'bookingSchedule',
    key: 'bookingSchedule',
    render: text => {
      const item = scheduleStrategies.find(x => x.code === text);
      return (<span>
        {item.name}&nbsp;
              <Tooltip title={item.description}>
          <Icon type="question-circle-o" />
        </Tooltip>
      </span>);
    }
  }, {
    title: 'Límites',
    dataIndex: 'bookingLimit',
    key: 'bookingLimit',
    render: text => {
      const item = limitStrategies.find(x => x.code === text);
      return (<span>
        {item.name}&nbsp;
              <Tooltip title={item.description}>
          <Icon type="question-circle-o" />
        </Tooltip>
      </span>);
    }
  }, {
    title: 'Anticipación',
    dataIndex: 'anticipationPolicy',
    key: 'anticipationPolicy',
    render: text => {
      const item = anticipationPolicies.find(x => x.code === text);
      return (<span>
        {item.name}&nbsp;
              <Tooltip title={item.description}>
          <Icon type="question-circle-o" />
        </Tooltip>
      </span>);
    }
  }, {
    title: 'Cancelación',
    dataIndex: 'cancellationPolicy',
    key: 'cancellationPolicy',
    render: text => {
      const item = cancellationPolicies.find(x => x.code === text);
      return (<span>
        {item.name}&nbsp;
              <Tooltip title={item.description}>
          <Icon type="question-circle-o" />
        </Tooltip>
      </span>);
    }
  }
]

class FacilityList extends Component {
  componentDidMount() {
    if (this.props.selectedConsortium !== null) {
      this.props.getFacilities();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.selectedConsortium !== prevProps.selectedConsortium) {
      this.props.getFacilities();
    }
  }

  render() {
    const data = this.props.facilities.map((item, key) => {
      const tmp = item.asMutable();
      tmp['key'] = key;
      tmp.createdOn = moment(tmp.createdOn).format('MMMM Do YYYY, HH:mm');
      return tmp;
    })
    return (
      <Card>
        <Table dataSource={data} columns={columns} loading={this.props.fetching} size="small" scroll={{ x: 700 }} />
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  facilities: state.facility.facilities,
  selectedConsortium: state.consortium.selectedConsortium,
  fetching: state.facility.isFetchingFacilities,
});

const mapDispatchToProps = dispatch => ({
  getFacilities: () => dispatch(FacilityActions.getFacilityListRequest()),
})

export default connect(mapStateToProps, mapDispatchToProps)(FacilityList);
