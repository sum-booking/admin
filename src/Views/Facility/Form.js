import React, { Component } from "react";
import { Form, Input, Button, Card, Alert, Icon, Select, Tooltip } from 'antd';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import FacilityActions from "../../Redux/FacilityRedux";
import { limitStrategies, scheduleStrategies, cancellationPolicies, anticipationPolicies } from "../../Services/Strategies";

const FormItem = Form.Item;

class FacilityForm extends Component {
  state = {
    conflict: false,
    limitDescription: 'Seleccione una opción para ver la descripción',
    scheduleDescription: 'Seleccione una opción para ver la descripción',
    cancellationDescription: 'Seleccione una opción para ver la descripción',
    anticipationDescription: 'Seleccione una opción para ver la descripción',
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, values) => {
      if (!err) {
        this.props.save(values);
      }
    });
  }

  onLimitChange = value => {
    const item = limitStrategies.find(x => x.code === value)
    this.setState({ limitDescription: item.description });
  }

  onScheduleChange = value => {
    const item = scheduleStrategies.find(x => x.code === value)
    this.setState({ scheduleDescription: item.description });
  }

  onCancellationChange = value => {
    const item = cancellationPolicies.find(x => x.code === value)
    this.setState({ cancellationDescription: item.description });
  }

  onAnticipationChange = value => {
    const item = anticipationPolicies.find(x => x.code === value)
    this.setState({ anticipationDescription: item.description });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isAdding && !this.props.isAdding && !this.props.message) {
      this.props.history.replace("/facility");
    }
    if (this.props.message !== prevProps.message && this.props.message === 409) {
      this.setState({ conflict: true });
    }
  }

  render() {
    const { getFieldDecorator, isAdding } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 4}
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 18}
      }
    };
    const submitButtonLayout = {
      wrapperCol: {
        span: 24,
        offset: 0
      }
    };
    const selectedConsortiumName = this.props.selectedConsortium ? this.props.selectedConsortium.name : null;
    return (
      <Card title={<span className="text-bold">Agregar amenitie</span>} type="inner">
        <span className="text-bold">Consorcio: {selectedConsortiumName}</span>
        <Form onSubmit={this.handleSubmit} style={{ marginTop: 20 }}>
          {this.state.conflict &&
            <Alert message="El nombre ya existe para otro amenitie" type="error" showIcon />
          }
          <FormItem label="Nombre:" {...formItemLayout}>
            {getFieldDecorator(`name`, {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un nombre'
                }
              ]
            })(<Input placeholder="Ingrese un nombre" maxLength={500} disabled={isAdding} />)}
          </FormItem>
          <FormItem label={(
            <span>
              Disponibilidad&nbsp;
              <Tooltip title={this.state.scheduleDescription}>
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )} {...formItemLayout}>
            {getFieldDecorator(`bookingSchedule`, {
              rules: [
                {
                  required: true,
                  message: 'Debe seleccionar una disponibilidad horaria'
                }
              ]
            })(<Select
              disabled={isAdding}
              style={{ width: '100%' }}
              onChange={this.onScheduleChange}
              placeholder="Seleccionar disponibilidad horaria" >
              {scheduleStrategies.map((item, key) =>
                <Select.Option key={key} value={item.code}>{item.name}</Select.Option>)}
            </Select>)}
          </FormItem>
          <FormItem label={(
            <span>
              Limite&nbsp;
              <Tooltip title={this.state.limitDescription}>
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
            {...formItemLayout}>
            {getFieldDecorator(`bookingLimit`, {
              rules: [
                {
                  required: true,
                  message: 'Debe seleccionar un límite de reserva'
                }
              ]
            })(<Select
              disabled={isAdding}
              style={{ width: '100%' }}
              onChange={this.onLimitChange}
              placeholder="Seleccionar límite de reserva" >
              {limitStrategies.map((item, key) =>
                <Select.Option key={key} value={item.code}>{item.name}</Select.Option>)}
            </Select>)}
          </FormItem>
          <FormItem label={(
            <span>
              Anticipación&nbsp;
              <Tooltip title={this.state.anticipationDescription}>
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
            {...formItemLayout}>
            {getFieldDecorator(`anticipationPolicy`, {
              rules: [
                {
                  required: true,
                  message: 'Debe seleccionar una política de anticipación'
                }
              ]
            })(<Select
              disabled={isAdding}
              style={{ width: '100%' }}
              onChange={this.onAnticipationChange}
              placeholder="Seleccionar una política de anticipación" >
              {anticipationPolicies.map((item, key) =>
                <Select.Option key={key} value={item.code}>{item.name}</Select.Option>)}
            </Select>)}
          </FormItem>
          <FormItem label={(
            <span>
              Cancelación&nbsp;
              <Tooltip title={this.state.cancellationDescription}>
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
            {...formItemLayout}>
            {getFieldDecorator(`cancellationPolicy`, {
              rules: [
                {
                  required: true,
                  message: 'Debe seleccionar una política de cancelación'
                }
              ]
            })(<Select
              disabled={isAdding}
              style={{ width: '100%' }}
              onChange={this.onCancellationChange}
              placeholder="Seleccionar una política de cancelación" >
              {cancellationPolicies.map((item, key) =>
                <Select.Option key={key} value={item.code}>{item.name}</Select.Option>)}
            </Select>)}
          </FormItem>
          <FormItem {...submitButtonLayout}>
            <Button type="primary" htmlType="submit" disabled={isAdding}>Guardar</Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  selectedConsortium: state.consortium.selectedConsortium,
  message: state.facility.addFailedMessage,
  isAdding: state.facility.isAdding
});

const maptDispatchToProps = dispatch => ({
  save: (values) => dispatch(FacilityActions.addFacilityRequest(values))
});

export default withRouter(connect(mapStateToProps, maptDispatchToProps)(Form.create()(FacilityForm)));
