import React, { Component } from 'react';
import matchMedia from "matchmediaquery";
import MediaQuery, {toQuery} from 'react-responsive';
import {
  Layout,
  Icon,
  Select,
  Menu,
  Dropdown,
  Avatar,
  Spin
} from 'antd';
import { connect } from "react-redux";
import ProfileActions from "../../Redux/ProfileRedux";
import ConsortiumActions from "../../Redux/ConsortiumRedux";

const logo = require('./logo.png');

class Header extends Component {
  constructor(props) {
    super(props);

    const mobileQuery = toQuery({ minDeviceWidth: 1224 });
    const mq = matchMedia(mobileQuery);
    mq.addListener(this.updateMenuState);
    
    if (!mq.matches && !props.collapsedMenu) {
      props.setMenuState(true);
    }
  }

  updateMenuState = value => {
    this.props.setMenuState(!value.matches);
  }

  logout = () => {
    this
      .props
      .logout();
    window.location.href = "/";
  }

  menu = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer">Perfil</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item >
        <a onClick={this.logout}>Cerrar sesión</a>
      </Menu.Item>
    </Menu>
  );  

  render() {
    return (
      <Layout.Header
        style={{
          position: 'fixed',
          width: '100%',
          padding: 0,
          zIndex: 3,
        }}>
        <MediaQuery minDeviceWidth={1224}>
          <div className="layout-logo" >
            <img src={logo} alt="Logo" />
          </div>
        </MediaQuery>
        <Icon
          type={this.props.collapsedMenu
            ? 'menu-unfold'
            : 'menu-fold'}
          onClick={this.props.toggleCollapsedMenu}
          className="menu-trigger" />
        <MediaQuery minDeviceWidth={1224}>
          {(matches) => (
            <Select
              showSearch
              style={{
                minWidth: matches ? 300 : "85%",
                marginLeft: "0.5em"
              }}
              size="large"
              placeholder={this.props.consortiumsFetching ? <Spin size="small" /> : "Seleccione un consorcio"}
              optionFilterProp="children"
              notFoundContent={this.props.consortiumsFetching ? <Spin size="small" /> : "No se encontraron consorcios"}
              value={this.props.selectedConsortium !== null ? this.props.selectedConsortium.code : undefined}
              disabled={this.props.consortiums.length === 0}
              onSelect={this.props.setSelectedConsortiumCode}
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
              {this.props.consortiums.map((item, key) => <Select.Option key={key} value={item.code}>{item.name}</Select.Option>)}
            </Select>
          )}
        </MediaQuery>
        <MediaQuery minDeviceWidth={1224}>
          <div style={{
            float: "right",
            marginRight: 25
          }}>
            <Dropdown overlay={this.menu} placement="bottomCenter">
              <Avatar
                src={this.props.pictureUrl}
                style={{
                  backgroundColor: "#fff"
                }}
                size="large" >{this.props.name}
              </Avatar>
            </Dropdown>
          </div>
        </MediaQuery>
      </Layout.Header>
    );
  }
}

const mapStateToProps = (state) => ({
  collapsedMenu: state.profile.collapsedMenu,
  pictureUrl: state.profile.pictureUrl,
  consortiums: state.consortium.consortiums,
  selectedConsortium: state.consortium.selectedConsortium,
  consortiumsFetching: state.consortium.isFetchingConsortiums,
  name: state.profile.name,
});
const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(ProfileActions.logout()),
  toggleCollapsedMenu: () => dispatch(ProfileActions.toggleCollapsedMenu()),
  setSelectedConsortiumCode: (value) => dispatch(ConsortiumActions.setSelectedConsortiumCode(value)),
  setMenuState: collapsed => dispatch(ProfileActions.setMenuState(collapsed)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);
