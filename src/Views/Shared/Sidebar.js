import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import { Layout, Menu } from 'antd';
import MdLocationCity from "react-icons/lib/md/location-city";
import MdHome from "react-icons/lib/md/home";
import FaBuilding from "react-icons/lib/fa/building";
import MdGroup from "react-icons/lib/md/group";
import { Link, withRouter } from "react-router-dom";
import pathToRegexp from 'path-to-regexp'
import { connect } from "react-redux";
import FaUserSecret from "react-icons/lib/fa/user-secret";
import FaLocalBar from "react-icons/lib/md/local-bar";
import MdBook from "react-icons/lib/md/book";

import routes from "../../routes";
import ProfileActions from "../../Redux/ProfileRedux";

const getCurrentMenu = (pathname) => {
  let name = "";
  for (let i = 0; i < routes.length; i++) {
    const item = routes[i];
    if (pathToRegexp(item.path).exec(pathname)) {
      name = item.name;
      break;
    }
  }
  return name;
}

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentMenu: getCurrentMenu(props.location.pathname),
      openKeys: []
    };
  }

  componentDidMount() {
    if (this.props.isRoot) {
      const openKeys = this.state.openKeys;
      openKeys.push("consortiums", "admins");
      this.setState({ openKeys })
    }

  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedConsortium === null && this.props.selectedConsortium) {
      const openKeys = this.state.openKeys;
      openKeys.push("functionalUnit", "user", "facility", "booking");
      this.setState({ openKeys })
    }
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.setState({ currentMenu: getCurrentMenu(this.props.location.pathname) });
    }
  }

  handleMenuClick = (matches) => {
    if (matches) {
      this.props.setMenuState(true);
    }
  }

  render() {
    return (
      <MediaQuery minDeviceWidth={1224}>
        {(matches) => (
          <Layout.Sider
            collapsedWidth="0"
            trigger={null}
            className="menu-sidebar"
            width={matches ? 250 : window.innerWidth}
            collapsed={this.props.collapsedMenu}>
            <Menu
              theme="dark"
              mode="inline"
              openKeys={this.state.openKeys}
              onOpenChange={openKeys => this.setState({ openKeys })}
              onClick={() => this.handleMenuClick(!matches)}
              selectedKeys={[this.state.currentMenu]}>
              <Menu.Item key="dashboard">
                <Link to="/dashboard">
                  <span><MdHome />
                    <span>Inicio</span>
                  </span>
                </Link>
              </Menu.Item>
              {this.props.isRoot &&
                [
                  <Menu.SubMenu
                    key="consortiums"
                    title={< span> <MdLocationCity /> <span> Consorcios </span></span>}>
                    <Menu.Item key="consortium_add">
                      <Link to="/consortium/add">Agregar</Link>
                    </Menu.Item>
                    <Menu.Item key="consortium_list">
                      <Link to="/consortium">Listar</Link>
                    </Menu.Item>
                  </Menu.SubMenu>,
                  <Menu.SubMenu
                    key="admins"
                    title={< span> <FaUserSecret /> <span> Administradores </span></span>}>
                    <Menu.Item key="admin_add">
                      <Link to="/admin/add">Agregar</Link>
                    </Menu.Item>
                    <Menu.Item key="admin_list">
                      <Link to="/admin">Listar</Link>
                    </Menu.Item>
                  </Menu.SubMenu>
                ]
              }
              <Menu.SubMenu
                disabled={!this.props.selectedConsortium}
                key="functionalUnit"
                title={< span > <FaBuilding /> < span > Unidades </span></span>}>
                <Menu.Item key="functionalUnit_add">
                  <Link to="/functionalunit/add">Agregar</Link>
                </Menu.Item>
                <Menu.Item key="functionalUnit_list">
                  <Link to="/functionalunit">Listar</Link>
                </Menu.Item>
              </Menu.SubMenu>
              <Menu.SubMenu
                disabled={!this.props.selectedConsortium}
                key="user"
                title={< span > <MdGroup /> < span > Usuarios </span></span>}>
                <Menu.Item key="user_add">
                  <Link to="/user/add">Agregar</Link>
                </Menu.Item>
                <Menu.Item key="user_list">
                  <Link to="/user">Listar</Link>
                </Menu.Item>
              </Menu.SubMenu>
              <Menu.SubMenu
                disabled={!this.props.selectedConsortium}
                key="facility"
                title={< span > <FaLocalBar /> < span > Amenities </span></span>}>
                <Menu.Item key="facility_add">
                  <Link to="/facility/add">Agregar</Link>
                </Menu.Item>
                <Menu.Item key="facility_list">
                  <Link to="/facility">Listar</Link>
                </Menu.Item>
              </Menu.SubMenu>
              <Menu.SubMenu
                disabled={!this.props.selectedConsortium}
                key="booking"
                title={< span > <MdBook /> < span > Reservas </span></span>}>
                <Menu.Item key="booking_list">
                  <Link to="/booking">Listar</Link>
                </Menu.Item>
              </Menu.SubMenu>
            </Menu>
          </Layout.Sider>
        )}
      </MediaQuery>
    );
  }
}

const mapStateToProps = (state) => ({
  collapsedMenu: state.profile.collapsedMenu,
  isRoot: state.profile.isRoot,
  selectedConsortium: state.consortium.selectedConsortium
});

const mapDispatchToProps = (dispatch) => ({
  setMenuState: state => dispatch(ProfileActions.setMenuState(state)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sidebar));
