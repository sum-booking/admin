import React, { Component } from 'react';
import { Layout } from 'antd';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import routes from "../../routes";
import { isAuthenticated } from "../../Redux/ProfileRedux";

const mapStateToProps = (state) => ({ isAuthenticated: isAuthenticated(state), collapsedMenu: state.profile.collapsedMenu });

const PrivateRoute = connect(mapStateToProps)(({ component: Component, isAuthenticated, ...rest }) => (
  <Route {...rest} render={(props) => (
    isAuthenticated
      ? <Component {...props} />
      : <Redirect to='/login' />
  )} />
))

class Content extends Component {
  state = {
    windowHeight: window.innerHeight
  };

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = (e) => {
    this.setState({ windowHeight: window.innerHeight })
  };
  render() {
    const minHeight = this.state.windowHeight - 100;
    return (
      <Layout.Content
        className={this.props.collapsedMenu
          ? "layout-content"
          : "layout-content layout-has-sider"}
        style={{
          minHeight: minHeight
        }}>
        <main>        
          <Switch>
            {routes.map((item, key) => <PrivateRoute
              key={key}
              path={item.path}
              name={item.name}
              component={item.component} />)}
            <Redirect from="/" to="/dashboard" />
          </Switch>
        </main>        
      </Layout.Content>
    );
  }
}

export default withRouter(connect(mapStateToProps)(Content));
