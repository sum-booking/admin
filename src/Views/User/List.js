import React, { Component } from "react";
import { Table, Card, notification } from "antd";
import { connect } from "react-redux";
import moment from "moment";

import UserActions from "../../Redux/UserRedux";

class AdminList extends Component {
  columns = [
    {
      title: 'Nombre',
      dataIndex: 'name',
      key: 'name',
      fixed: 'left'
    }, {
      title: 'Correo',
      dataIndex: 'email',
      key: 'email'
    }, {
      title: 'Teléfono',
      dataIndex: 'phoneNumber',
      key: 'phoneNumber'
    }, {
      title: 'Unidad',
      dataIndex: 'functionalUnit',
      key: 'functionalUnit'
    }, {
      title: 'Acciones',
      key: 'action',
      render: (text, record) => (
        <span>
          <a onClick={() => this.props.resendAuthLink(record.functionalUnitCode, record.userId)}>Enviar link</a>
        </span>
      )
    }
  ]

  componentDidMount() {
    if (this.props.selectedConsortium !== null) {
      this.props.getUsers();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.selectedConsortium !== prevProps.selectedConsortium) {
      this.props.getUsers();
    }
    if (prevProps.isResendingAuthLink !== this.props.isResendingAuthLink && !this.props.isResendingAuthLink){
      if (this.props.authLinkFailedMessage) {
        notification["error"]({
          message: 'Error enviando link',
          description: 'Hubo un error al enviar el link, intente más tarde'
        });
      } else {
        notification["success"]({
          message: 'Link enviado',
          description: 'El link se ha enviado con éxito'
        });
      }
    }
  }

  render() {
    const data = this.props.users.map((item, key) => {
      const tmp = item.asMutable();
      tmp.key = key;
      tmp.createdOn = moment(tmp.createdOn).format('MMMM Do YYYY, HH:mm');
      tmp.name = `${tmp.firstName} ${tmp.lastName}`;
      return tmp;
    })
    return (
      <Card>
        <Table dataSource={data} columns={this.columns} loading={this.props.fetching} size="small" scroll={{ x: 700 }} />
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  users: state.user.users,
  fetching: state.user.isFetchingUsers,
  selectedConsortium: state.consortium.selectedConsortium,
  isResendingAuthLink: state.user.isResendingAuthLink,
  authLinkFailedMessage: state.user.authLinkFailedMessage,
});

const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(UserActions.getUserListRequest()),
  resendAuthLink: (functionalUnitCode, userId) => dispatch(UserActions.resendUserAuthLinkRequest(functionalUnitCode, userId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(AdminList);
