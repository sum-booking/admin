import React, { Component } from "react";
import { Form, Input, Button, Card, Alert, Select, Switch } from 'antd';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import UserActions from "../../Redux/UserRedux";
import FunctionalUnitActions from "../../Redux/FunctionalUnitRedux";

const FormItem = Form.Item;

class UserForm extends Component {
  state = {
    conflict: false,
    sendWelcomingMail: true,
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this
      .props
      .form
      .validateFields((err, values) => {
        if (!err) {
             this.props.save(values.firstName, values.lastName, values.email,
              values.phoneNumber, values.functionalUnitCode, this.state.sendWelcomingMail);
        }
      });
  }
  componentDidMount() {
    if (this.props.selectedConsortium !== null) {
      this.props.getFunctionalUnits();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isAdding && !this.props.isAdding && !this.props.message) {
      this.props.history.replace("/user");
    }
    if (this.props.message !== prevProps.message && this.props.message === 409) {
      this.setState({ conflict: true });
    }
    if (this.props.selectedConsortium !== prevProps.selectedConsortium) {
      this.props.getFunctionalUnits();
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { isAdding, selectedConsortium } = this.props;
    const formItemLayout = {
      labelCol: {
        xs: {span: 24},
        sm: {span: 6}
      },
      wrapperCol: {
        xs: {span: 24},
        sm: {span: 10}
      }
    };
    const submitButtonLayout = {
      wrapperCol: {
        xs: {span: 6, offset: 8},
        sm: {span: 10, offset: 6}
      }
    };
    const consortiumName = selectedConsortium ? selectedConsortium.name : "";
    return (
      <Card title={<span className="text-bold">Agregar usuario</span>} type="inner">
        {this.state.conflict &&
          <Alert message="El correo electrónico ya existe para otro usuario" type="error" showIcon />
        }
        <Form onSubmit={this.handleSubmit} style={{ marginTop: 20 }}>
          <FormItem label="Consorcio:" {...formItemLayout}>
            <Input maxLength={500} disabled value={consortiumName} />
          </FormItem>
          <FormItem label="Nombre:" {...formItemLayout}>
            {getFieldDecorator('firstName', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un nombre'
                }
              ]
            })(<Input placeholder="Ingrese un nombre" maxLength={500} disabled={isAdding} autoFocus />)}
          </FormItem>
          <FormItem label="Apellido:" {...formItemLayout}>
            {getFieldDecorator('lastName', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un apellido'
                }
              ]
            })(<Input placeholder="Ingrese un apellido" maxLength={500} disabled={isAdding} />)}
          </FormItem>
          <FormItem label="Correo electrónico:" {...formItemLayout}>
            {getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un correo electrónico'
                }
              ]
            })(<Input placeholder="Ingrese un correo electrónico" disabled={isAdding} maxLength={500} type="email" />)}
          </FormItem>
          <FormItem label="Teléfono:" {...formItemLayout}>
            {getFieldDecorator('phoneNumber', {
              rules: [
                {
                  required: true,
                  message: 'Debe ingresar un teléfono'
                }
              ]
            })(<Input placeholder="Ingrese un teléfono" disabled={isAdding} maxLength={500} />)}
          </FormItem>
          <FormItem label="Unidad funcional" {...formItemLayout}>
            {getFieldDecorator('functionalUnitCode', {
              rules: [
                {
                  required: true,
                  message: 'Debe seleccionar una unidad funcional'
                }
              ]
            })(<Select
              showSearch
              style={{ width: '100%' }}
              placeholder="Elegir unidad funcional"
              filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
              {this.props.functionalUnits.map((item, key) =>
                <Select.Option key={key} value={item.code}>{item.designation}</Select.Option>)}
            </Select>)}
          </FormItem>
          <FormItem label="Enviar acceso a la aplicación:" {...formItemLayout}>
            <Switch defaultChecked onChange={value => this.setState({ sendWelcomingMail: value })} />
          </FormItem>
          <FormItem {...submitButtonLayout}>
            <Button type="primary" htmlType="submit" disabled={isAdding}>Guardar</Button>
          </FormItem>
        </Form>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  functionalUnits: state.functionalUnit.functionalUnits,
  selectedConsortium: state.consortium.selectedConsortium,
  message: state.user.addFailedMessage,
  isAdding: state.user.isAdding
});

const maptDispatchToProps = dispatch => ({
  getFunctionalUnits: () => dispatch(FunctionalUnitActions.getFunctionalUnitListRequest()),
  save: (firstname, lastName, email, phoneNumber, functionalUnitCode, sendWelcomingMail) =>
    dispatch(UserActions.addUserRequest(firstname, lastName, email, phoneNumber, functionalUnitCode, sendWelcomingMail))
});

export default withRouter(connect(mapStateToProps, maptDispatchToProps)(Form.create()(UserForm)));
