import React, {Component} from 'react';
import {Layout as ALayout} from 'antd';
import { connect } from "react-redux";

import StartupActions from "../Redux/StartupRedux";
import Sidebar from "./Shared/Sidebar";
import Header from "./Shared/Header";
import Content from "./Shared/Content";

import "../Styles/Layout.css";

class Layout extends Component {
  componentDidMount() {
    this.props.startup();
  }

  render() {
    return (
      <ALayout>
        <Sidebar/>
        <ALayout>
          <Header/>
          <ALayout>
            <Content/>
          </ALayout>
        </ALayout>
      </ALayout>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup())
})
export default connect(null, mapDispatchToProps)(Layout);
