import React from "react";
import Async from 'react-code-splitting';

const Dashboard = props => <Async load={import("./Views/Home/Dashboard")} componentProps={props} />;
const ConsortiumForm = props => <Async load={import("./Views/Consortium/Form")} componentProps={props} />;
const ConsortiumList = props => <Async load={import("./Views/Consortium/List")} componentProps={props} />;
const AdminForm = props => <Async load={import("./Views/Admin/Form")} componentProps={props} />;
const AdminList = props => <Async load={import("./Views/Admin/List")} componentProps={props} />;
const FunctionalUnitForm = props => <Async load={import("./Views/FunctionalUnit/Form")} componentProps={props} />;
const FunctionalUnitList = props => <Async load={import("./Views/FunctionalUnit/List")} componentProps={props} />;
const UserForm = props => <Async load={import("./Views/User/Form")} componentProps={props} />;
const UserList = props => <Async load={import("./Views/User/List")} componentProps={props} />;
const FacilityForm = props => <Async load={import("./Views/Facility/Form")} componentProps={props} />;
const FacilityList = props => <Async load={import("./Views/Facility/List")} componentProps={props} />;
const BookingList = props => <Async load={import("./Views/Booking/List")} componentProps={props} />;

const routes = [
  {
    path: "/dashboard",
    component: Dashboard,
    exact: true,
    name: "dashboard"
  }, {
    path: "/consortium/add",
    component: ConsortiumForm,
    name: "consortium_add"
  }, {
    path: "/consortium",
    component: ConsortiumList,
    name: "consortium_list"
  }, {
    path: "/admin/add",
    component: AdminForm,
    name: "admin_add"
  }, {
    path: "/admin",
    component: AdminList,
    name: "admin_list"
  }, {
    path: "/functionalunit/add",
    component: FunctionalUnitForm,
    name: "functionalUnit_add"
  }, {
    path: "/functionalunit",
    component: FunctionalUnitList,
    name: "functionalUnit_list"
  }, {
    path: "/user/add",
    component: UserForm,
    name: "user_add"
  }, {
    path: "/user",
    component: UserList,
    name: "user_list"
  }, {
    path: "/facility/add",
    component: FacilityForm,
    name: "facility_add"
  }, {
    path: "/facility",
    component: FacilityList,
    name: "facility_list"
  }, {
    path: "/booking",
    component: BookingList,
    name: "booking_list"
  }
]

export default routes;
