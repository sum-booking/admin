import { create } from 'apisauce'

// define the api
export default create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
  headers: { 'Accept': 'application/json' },
});


