export const scheduleStrategies = [
  {code: 'NightOnly', name: 'Noche', description: 'Solo se puede reservar durante la noche'},
  {code: 'MidDayAndNight', name: 'Dia y noche', description: 'Se puede reservar al mediodia y a la noche'},
];

export const limitStrategies = [
  {code: 'TwiceAMonth', name: '2 veces por mes', description: 'Solo se puede reservar dos veces al mes por una misma unidad funcional'},
];

export const cancellationPolicies = [
  {code: 'FortyEightHours', name: '48 horas antes', description: 'La reserva del amenitie puede ser cancelada hasta 48 horas antes'},
  {code: 'SeventyTwoHours', name: '72 horas antes', description: 'La reserva del amenitie puede ser cancelada hasta 72 horas antes'},  
]

export const anticipationPolicies = [
  {code: 'TwoDays', name: '2 días', description: 'La reserva del amenitie se puede realizar con un mínimo de 2 días de anticipación'},
  {code: 'SevenDays', name: '7 días', description: 'La reserva del amenitie se puede realizar con un mínimo de 7 días de anticipación'},
]
