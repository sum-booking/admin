import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'
import { LocaleProvider } from 'antd';
import es_ES from 'antd/lib/locale-provider/es_ES';
import 'moment/locale/es';
import Async from 'react-code-splitting';

import configureStore from './Redux/createStore';
import registerServiceWorker from './registerServiceWorker';

const Login = props => <Async load={import('./Views/Auth/Login')} componentProps={props} />;
const Callback = props => <Async load={import('./Views/Auth/Callback')} componentProps={props} />;
const Layout = props => <Async load={import('./Views/Layout')} componentProps={props} />;

const { store, persistor } = configureStore();

ReactDOM.render(
  <LocaleProvider locale={es_ES}>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router>
          <Switch>
            <Route exact path="/login" name="login" component={Login} />
            <Route exact path="/callback" name="callback" component={Callback} />
            <Route path="/" name="layout" component={Layout} />
          </Switch>
        </Router>
      </PersistGate>
    </Provider>
  </LocaleProvider>, document.getElementById('root'));
registerServiceWorker();
