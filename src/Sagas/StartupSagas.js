import { select, call, put } from "redux-saga/effects";
import { getState, getAccessToken } from "./Selectors";
import { isAuthenticated } from "../Redux/ProfileRedux";
import { setApiAuthorization } from "./ProfileSagas";
import ConsortiumActions from "../Redux/ConsortiumRedux";
import AdminActions from "../Redux/AdminRedux";

export function* startup() {
  const state = yield select(getState);
  const authenticated = isAuthenticated(state);
  if (authenticated) {
    const accessToken = yield select(getAccessToken);
    yield call(setApiAuthorization, { accessToken });
    yield put(ConsortiumActions.getConsortiumListRequest());
    yield put(AdminActions.getAdminListRequest());
  }
}
