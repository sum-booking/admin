import { select, put, call } from "redux-saga/effects";
import Raven from "raven-js";

import FacilityActions from "../Redux/FacilityRedux";
import Api from "../Services/Api";

import { getUserEmail, getSelectedConsortiumCode } from "./Selectors";

export function* addFacilities({ values }) {
  const { name, bookingSchedule, bookingLimit, cancellationPolicy, anticipationPolicy } = values;
  const consortiumCode = yield select(getSelectedConsortiumCode);
  const createdBy = yield select(getUserEmail);
  const createdOn = new Date();

  try {
    const response = yield call([Api, 'post'], `consortium/${consortiumCode}/facility`, {
      name,
      bookingSchedule,
      bookingLimit,
      cancellationPolicy,
      anticipationPolicy,
      createdBy,
      createdOn
    });
    if (response.ok) {
      yield put(FacilityActions.addFacilityResponse());
    }
    else {
      yield put(FacilityActions.addFacilityFailed(response.status));
    }
  } catch (err) {
    Raven.captureException(err);
    yield put(FacilityActions.addFacilityFailed(err));
  }
}

export function* getFacilityList() {
  try {
    const consortiumCode = yield select(getSelectedConsortiumCode);
    const response = yield call([Api, 'get'], `consortium/${consortiumCode}/facility`);
    if (response.ok) {
      yield put(FacilityActions.getFacilityListResponse(response.data));
    } else if (response.status === 404) {
      yield put(FacilityActions.getFacilityListResponse([]));
    } else {
      yield put(FacilityActions.getFacilityListFailed(response.problem));
    }
  } catch (err) {
    Raven.captureException(err);
    yield put(FacilityActions.getFacilityListFailed(err));
  }
}
