export const getUserEmail = state => state.profile.email;
export const getState = state => state;
export const getAccessToken = state => state.profile.accessToken;
export const getSelectedConsortiumCode = state => state.consortium.selectedConsortium.code;
