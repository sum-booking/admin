import { all, takeLatest } from 'redux-saga/effects';

import { StartupTypes } from "../Redux/StartupRedux";
import { ProfileTypes } from "../Redux/ProfileRedux";
import { ConsortiumTypes } from "../Redux/ConsortiumRedux";
import { AdminTypes } from "../Redux/AdminRedux";
import { FunctionalUnitTypes } from "../Redux/FunctionalUnitRedux";
import { UserTypes } from "../Redux/UserRedux";
import { FacilityTypes } from "../Redux/FacilityRedux";
import { BookingTypes } from "../Redux/BookingRedux";

import { renewToken, setApiAuthorization } from "./ProfileSagas";
import { startup } from "./StartupSagas";
import { getConsortiumList, addConsortium } from "./ConsortiumSagas";
import { getAdminList, addAdmin } from "./AdminSagas";
import { addFunctionalUnits, getFunctionalUnitList } from './FunctionalUnitSagas';
import { addUser, getUserList, resendUserAuthLink } from './UserSagas';
import { addFacilities, getFacilityList} from "./FacilitySagas";
import { getBookingList } from "./BookingSagas";

export default function* root() {
  yield all([
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(ProfileTypes.LOGIN, renewToken),
    takeLatest(ProfileTypes.LOGIN, setApiAuthorization),
    takeLatest(ProfileTypes.UPDATE_TOKEN, renewToken),
    takeLatest(ProfileTypes.LOGIN, setApiAuthorization),
    takeLatest(ConsortiumTypes.GET_CONSORTIUM_LIST_REQUEST, getConsortiumList),
    takeLatest(ConsortiumTypes.ADD_CONSORTIUM_REQUEST, addConsortium),
    takeLatest(AdminTypes.ADD_ADMIN_REQUEST, addAdmin),
    takeLatest(AdminTypes.GET_ADMIN_LIST_REQUEST, getAdminList),
    takeLatest(FunctionalUnitTypes.ADD_FUNCTIONAL_UNITS_REQUEST, addFunctionalUnits),
    takeLatest(FunctionalUnitTypes.GET_FUNCTIONAL_UNIT_LIST_REQUEST, getFunctionalUnitList),
    takeLatest(UserTypes.ADD_USER_REQUEST, addUser),
    takeLatest(UserTypes.GET_USER_LIST_REQUEST, getUserList), 
    takeLatest(UserTypes.RESEND_USER_AUTH_LINK_REQUEST, resendUserAuthLink),
    takeLatest(FacilityTypes.ADD_FACILITY_REQUEST, addFacilities),
    takeLatest(FacilityTypes.GET_FACILITY_LIST_REQUEST, getFacilityList),
    takeLatest(BookingTypes.GET_BOOKING_LIST_REQUEST, getBookingList),
  ]);
}
