import { select, put, call } from "redux-saga/effects";
import Raven from "raven-js";

import ConsortiumActions from "../Redux/ConsortiumRedux";
import Api from "../Services/Api";

import { getUserEmail } from "./Selectors";

export function* addConsortium({ code, name }) {
  const createdBy = yield select(getUserEmail);
  const createdOn = new Date();

  try {
    const response = yield call([Api, 'post'], "consortium", {
      name,
      code,
      createdBy,
      createdOn
    });
    if (response.ok) {
      yield put(ConsortiumActions.addConsortiumResponse());
      yield put(ConsortiumActions.getConsortiumListRequest());
    } else {
      yield put(ConsortiumActions.addConsortiumFailed(response.status));
    }
  }
  catch (err) {
    Raven.captureException(err);
    yield put(ConsortiumActions.addConsortiumFailed(err));
  }
}

export function* getConsortiumList() {
  try {
    const response = yield call([Api, 'get'], 'consortium/');
    if (response.ok) {
      yield put(ConsortiumActions.getConsortiumListResponse(response.data));
    } else {
      yield put(ConsortiumActions.getConsortiumListFailed(response.problem));
    }
  } catch (err) {
    Raven.captureException(err);
    yield put(ConsortiumActions.getConsortiumListFailed(err));
  }
}
