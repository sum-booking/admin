import { select, put, call } from "redux-saga/effects";
import Raven from "raven-js";

import UserActions from "../Redux/UserRedux";
import Api from "../Services/Api";

import { getUserEmail, getSelectedConsortiumCode } from "./Selectors";

export function* addUser({ firstName, lastName, email, phoneNumber, functionalUnitCode, sendWelcomingMail }) {
  const createdBy = yield select(getUserEmail);
  const consortiumCode = yield select(getSelectedConsortiumCode);
  const createdOn = new Date();

  try {
    const response = yield call([Api, 'post'], `consortium/${consortiumCode}/functionalunit/${functionalUnitCode}/user`, {
      firstName,
      lastName,
      email,
      phoneNumber,
      sendWelcomingMail,
      phoneNotifications: true,
      emailNotifications: true,
      createdBy,
      createdOn
    });
    if (response.ok) {
      yield put(UserActions.addUserResponse());
      yield put(UserActions.getUserListRequest());
    } else {
      yield put(UserActions.addUserFailed(response.status));
    }
  }
  catch (err) {
    Raven.captureException(err);
    yield put(UserActions.addUserFailed(err));
  }
}

export function* getUserList() {
  try {
    const consortiumCode = yield select(getSelectedConsortiumCode);
    const response = yield call([Api, 'get'], `consortium/${consortiumCode}/user`);
    if (response.ok) {
      yield put(UserActions.getUserListResponse(response.data));
    } else {
      yield put(UserActions.getUserListFailed(response.problem));
    }
  } catch (err) {
    Raven.captureException(err);
    yield put(UserActions.getUserListFailed(err));
  }
}

export function* resendUserAuthLink({ functionalUnitCode, userId}) {
  try {
    const consortiumCode = yield select(getSelectedConsortiumCode);
    const response = yield call([Api, 'post'], `consortium/${consortiumCode}/functionalunit/${functionalUnitCode}/user/resendlink/${userId}/`);
    if (response.ok) {
      yield put(UserActions.resendUserAuthLinkResponse());
    } else {
      yield put(UserActions.resendUserAuthLinkFailed(response.problem));
    }
  } catch (err) {
    Raven.captureException(err);
    yield put(UserActions.resendUserAuthLinkFailed(err));
  }
}
