import { select, put, call } from "redux-saga/effects";
import Raven from "raven-js";

import FunctionalUnitActions from "../Redux/FunctionalUnitRedux";
import Api from "../Services/Api";

import { getUserEmail, getSelectedConsortiumCode } from "./Selectors";

export function* addFunctionalUnits({ values }) {
  const { keys, codes, designations } = values;
  const consortiumCode = yield select(getSelectedConsortiumCode);
  const createdBy = yield select(getUserEmail);
  const createdOn = new Date();

  const saved = [];
  const error = [];
  for (var i = 0; i < keys.length; i++) {
    const key = keys[i];
    const code = codes[key];
    const designation = designations[key];
    try {
      const response = yield call([Api, 'post'], `consortium/${consortiumCode}/functionalunit`, {
        designation,
        code,
        createdBy,
        createdOn
      });
      if (response.ok) {
        saved.push({ key });
      }
      else {
        error.push({ key, message: response.status });
      }
    } catch (err) {
      Raven.captureException(err);
      error.push({ key, message: err });
    }
  }
  yield put(FunctionalUnitActions.addFunctionalUnitsResponse(saved));
  yield put(FunctionalUnitActions.addFunctionalUnitsFailed(error));
}

export function* getFunctionalUnitList() {
  try {
    const consortiumCode = yield select(getSelectedConsortiumCode);
    const response = yield call([Api, 'get'], `consortium/${consortiumCode}/functionalunit`);
    if (response.ok) {
      yield put(FunctionalUnitActions.getFunctionalUnitListResponse(response.data));
    } else if (response.status === 404) {
      yield put(FunctionalUnitActions.getFunctionalUnitListResponse([]));
    } else {
      yield put(FunctionalUnitActions.getFunctionalUnitListFailed(response.problem));
    }
  } catch (err) {
    Raven.captureException(err);
    yield put(FunctionalUnitActions.getFunctionalUnitListFailed(err));
  }
}
