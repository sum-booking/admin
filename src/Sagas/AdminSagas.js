import { select, put, call } from "redux-saga/effects";
import Raven from "raven-js";

import AdminActions from "../Redux/AdminRedux";
import Api from "../Services/Api";

import { getUserEmail } from "./Selectors";

export function* addAdmin({ firstName, lastName, email, phoneNumber, consortiumCodes }) {
  const createdBy = yield select(getUserEmail);
  const createdOn = new Date();

  try {
    const response = yield call([Api, 'post'], "admin", {
      firstName,
      lastName,
      email,
      phoneNumber,
      consortiumCodes,
      createdBy,
      createdOn
    });
    if (response.ok) {
      yield put(AdminActions.addAdminResponse());
      yield put(AdminActions.getAdminListRequest());
    } else {
      yield put(AdminActions.addAdminFailed(response.status));
    }
  }
  catch (err) {
    Raven.captureException(err);
    yield put(AdminActions.addAdminFailed(err));
  }
}

export function* getAdminList() {
  try {
    const response = yield call([Api, 'get'], 'admin');
    if (response.ok) {
      yield put(AdminActions.getAdminListResponse(response.data));
    } else {
      yield put(AdminActions.getAdminListFailed(response.problem));
    }
  } catch (err) {
    Raven.captureException(err);
    yield put(AdminActions.getAdminListFailed(err));
  }
}
