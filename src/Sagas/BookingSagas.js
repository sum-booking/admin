import { select, put, call } from "redux-saga/effects";
import Raven from "raven-js";

import BookingActions from "../Redux/BookingRedux";
import Api from "../Services/Api";

import { getSelectedConsortiumCode } from "./Selectors";

export function* getBookingList({from, to}) {
  try {
    const consortiumCode = yield select(getSelectedConsortiumCode);
    const response = yield call([Api, 'get'], `consortium/${consortiumCode}/booking`, {from, to});
    if (response.ok) {
      yield put(BookingActions.getBookingListResponse(response.data));
    } else if (response.status === 404) {
      yield put(BookingActions.getBookingListResponse([]));
    } else {
      yield put(BookingActions.getBookingListFailed(response.problem));
    }
  } catch (err) {
    Raven.captureException(err);
    yield put(BookingActions.getBookingListFailed(err));
  }
}
