import { put, call } from 'redux-saga/effects';
import { delay } from "redux-saga";
import Auth from "../Services/Auth";
import ProfileActions from "../Redux/ProfileRedux";
import Api from "../Services/Api";

const TIMEOUT_THRESHOLD = 5 * 1000 * 60;

export function* renewToken({ expiresAt }) {
  const timeout = expiresAt - TIMEOUT_THRESHOLD - new Date().getTime();
  yield delay(timeout);
  try {
    const authResult = call(checkSession);
    const newExpiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    yield put(ProfileActions.updateToken(authResult.updateToken, authResult.idToken, newExpiresAt))
  } catch (err) {
    yield put(ProfileActions.error(err));
  }
}

export function* setApiAuthorization({ accessToken }) {
  yield call([Api, 'setHeader'], 'Authorization', `Bearer ${accessToken}`);
}

const checkSession = () => {
  return new Promise((resolve, reject) => {
    Auth
      .checkSession({
        audience: process.env.REACT_APP_AUTH0_API_IDENTIFIER,
        scope: 'openid profile email'
      }, function (err, authResult) {
        if (authResult && authResult.accessToken && authResult.idToken) {
          resolve(authResult);
        } else if (err) {
          reject(err);
        }
      });
  });
}
